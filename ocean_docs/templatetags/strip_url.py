from django import template

register = template.Library()


@register.filter
def static_url_to_upload_url(static_url_str, append_url):
	append_url = [append_url]
	url_list = static_url_str.split('/')
	append_url.extend(url_list[2:])
	upload_url = '/'.join(append_url)
	return upload_url


@register.filter
def upload_url_to_static_url(upload_url_str, append_url):
	append_url = [append_url]
	url_list = upload_url_str.split('/')
	append_url.extend(url_list[2:])
	media_url = '/'.join(append_url)
	return media_url


@register.filter
def previous_dir_url(current_dir_url):
	url_list = current_dir_url.split('/')
	if len(url_list) == 5:
		previous_dir_url = '/'.join(url_list[:-3])
	else:
		previous_dir_url = '/'.join(url_list[:-2])
	return previous_dir_url


@register.filter
def full_path_file(dir_name, file_name):
	dir_list = dir_name.split('media')[1:]
	dir_list.append(file_name)
	file_path = '/'.join(dir_list)
	return file_path


@register.filter
def static_to_delete_url(url, content):
	#import ipdb; ipdb.set_trace()
	url_list = url.split('/')
	delete_url = ["/delete_dir"]
	delete_url.extend(url_list[2:])
	delete_url = delete_url[:-1]
	delete_url.append(content.name)
	try:
		delete_url.append(content.enquiry_id)
	except:
		pass
	delete_url = '/'.join(delete_url)
	return delete_url


@register.filter
def static_to_doc_delete_url(url, content):
	#import ipdb; ipdb.set_trace()
	url_list = url.split('/')
	delete_url = ["/delete_docs"]
	delete_url.extend(url_list[2:])
	delete_url = delete_url[:-1]
	delete_url.append(content.file_name)
	delete_url = '/'.join(delete_url)
	return delete_url