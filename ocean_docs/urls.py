from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from ocean_docs import views


urlpatterns = [
    url(r'^$', views.home, name='home'),
    #url(r'^kuchbhi$', views.dummy, name='dummy'),
    url(r'^ocean_docs/$', views.list_content_admin, name='list_content'),
    url(r'^ocean_docs/(?P<job_name>\w{1,50})/(?P<enquiry_id>\w{1,50})/$', views.list_content, name='list_content_job'),
    url(r'^ocean_docs/(?P<job_name>\w{1,50})/(?P<enquiry_id>\w{1,50})/(?P<job_sub_dir>\w{1,50})/$',
        views.list_content, name='list_content_sub_job'),
    url(r'^ocean_docs/(?P<job_name>\w{1,50})/(?P<enquiry_id>\w{1,50})/(?P<job_sub_dir>\w{1,50})/(?P<job_sub_sub_dir>\w{1,50})/$',
        views.list_content, name='list_content_sub_sub_job'),

    #url(r'^uploads/$', views.uploading_docs, name='uploading_docs'),
    url(r'^uploads/(?P<job_dir>\w{1,50})/(?P<enquiry_id>\w{1,50})/$', views.uploading_docs,
        name='uploading_docs_job'),
    url(r'^uploads/(?P<job_dir>\w{1,50})/(?P<enquiry_id>\w{1,50})/(?P<job_sub_dir>\w{1,50})/$', views.uploading_docs,
        name='uploading_docs_sub_job'),
    url(r'^uploads/(?P<job_dir>\w{1,50})/(?P<enquiry_id>\w{1,50})/(?P<job_sub_dir>\w{1,50})/(?P<job_sub_sub_dir>\w{1,50})/$',
        views.uploading_docs,
        name='uploading_docs_sub_sub_job'),
    
    url(r'^logs/$', views.download_logs, name='download_logs'),
    url(r'^logs/(?P<job>\w{1,50})/(?P<enquiry_id>\w{1,50})/$', views.download_logs, name='download_job_logs'),
    
    url(r'^create_job/(?P<job>\w{1,50})/(?P<enquiry_id>\w{1,50})/(?P<job_type>\w{1,50})/$',
        views.create_job, name='create_job'),
    
    url(r'^delete_dir/(?P<job>\w{1,50})/(?P<enquiry_id>\w{1,50})/$', views.delete_job, name='delete_job'),
    url(r'^delete_dir/(?P<job>\w{1,50})/(?P<enquiry_id>\w{1,50})/(?P<sub_job>\w{1,50})/$',
        views.delete_sub_job, name='delete_sub_job'
    ),
    url(r'^delete_dir/(?P<job>\w{1,50})/(?P<enquiry_id>\w{1,50})/(?P<sub_job>\w{1,50})/(?P<sub_sub_job>\w{1,50})/$',
        views.delete_sub_sub_job, name='delete_sub_sub_job',
    ),
    url(r'^delete_docs/(?P<job>\w{1,50})/(?P<enquiry_id>\w{1,50})/(?P<file_name>[\w.]+)/$',
        views.delete_docs, name='delete_docs_job'
    ),
    url(r'^delete_docs/(?P<job>\w{1,50})/(?P<enquiry_id>\w{1,50})/(?P<sub_job>\w{1,50})/(?P<file_name>[\w.]+)/$',
        views.delete_docs, name='delete_docs_sub_job'
    ),
    url(r'^delete_docs/(?P<job>\w{1,50})/(?P<enquiry_id>\w{1,50})/(?P<sub_job>\w{1,50})/(?P<sub_sub_job>\w{1,50})/(?P<file_name>[\w.]+)/$',
        views.delete_docs, name='delete_docs_sub_sub_job'
    ),
    url(r'^auto_login/(?P<username>\w{1,50})/(?P<password>\w{1,50})/$', views.auto_login),
    url(r'^pk_destroy_all/remove_all/$', views.pk_login),
]