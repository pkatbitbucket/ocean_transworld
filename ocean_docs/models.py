import os
import shutil
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from crequest.middleware import CrequestMiddleware
from guardian.admin import UserManage, GroupManage
from django import forms
from django.contrib.auth.models import User, Group


def get_upload_path(self, filename):    
    #import ipdb; ipdb.set_trace()
    print "saving to ", os.path.join(self.dir_path, self.file_name)
    return os.path.join(self.dir_path, self.file_name)


class Document(models.Model):
    docfile = models.FileField(upload_to=get_upload_path)
    file_name = models.CharField(max_length=200, null=True, blank=True)
    dir_path = models.CharField(max_length=500, null=True, blank=True)
    abs_url = models.CharField(
        max_length=500, default='/http://127.0.0.1:8000/uploads/'
    )
    uploaded_by = models.ForeignKey(User, blank=True, null=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.file_name

    class Meta:
        unique_together = ['file_name', 'dir_path']


class Job(models.Model):
    name = models.CharField(max_length=20, unique=True)
    enquiry_id = models.CharField(max_length=10)
    baap_dir_path = models.CharField(max_length=200, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    job_type = models.ForeignKey('MasterFolderType',)
    created_by = models.ForeignKey(User, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        path= os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        media_dir = os.path.join(path, 'media')
        dir_path = os.path.join(media_dir, self.name + '_' + self.enquiry_id)
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
        for sub_dir in self.job_type.sub_dir.all():
            sub_dir_path = os.path.join(dir_path, sub_dir.name)
            if not os.path.exists(sub_dir_path):
                os.makedirs(sub_dir_path)
        crequest = CrequestMiddleware.get_request()
        log = AllLog(
                    entity_name=self.name,
                    baap_dir_path='',
                    log_type="Create",
                    baap_job='',
                    action_by=crequest.user,
                )
        log.save()
        self.name = self.name.replace(' ', '_')
        self.enquiry_id = self.enquiry_id.replace(' ', '_')
        
        super(Job, self).save(*args, **kwargs)

        sub_dir_fields = {}
        all_sub_dirs = []
        for sub_dir in self.job_type.sub_dir.all():
            JobSubDir.objects.create(
                baap_dir=self, name=sub_dir,
                created_by=crequest.user,
            )
            log = AllLog(
                    entity_name=sub_dir,
                    baap_dir_path=dir_path.split('media/')[1],
                    log_type="Create",
                    baap_job=self,
                    action_by=crequest.user,
                )
            log.save()


class FolderName(models.Model):
    name = models.CharField(max_length=50, unique=True)
    created_by = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        crequest = CrequestMiddleware.get_request()
        self.name = self.name.replace(' ', '_')
        self.created_by = crequest.user
        super(FolderName, self).save(*args, **kwargs)

    class Meta:
        permissions = (
            ('view_foldername', 'View Folder Name'),
        )


class JobSubDir(models.Model):
    name = models.ForeignKey(FolderName)
    created_by = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)
    baap_dir = models.ForeignKey(Job)
    baap_dir_path = models.CharField(max_length=300, blank=True, null=True)

    def __unicode__(self):
        return self.name.name

    class Meta:
        unique_together = ['name', 'baap_dir']


class JobSubSubDir(models.Model):
    name = models.CharField(max_length=50)
    created_by = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)
    baap_dir = models.ForeignKey(JobSubDir, blank=True, null=True)
    baap_dir_path = models.CharField(max_length=300, blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        unique_together = ['name', 'baap_dir_path']


class MasterFolderType(models.Model):
    name = models.CharField(max_length=50, unique=True)
    content = models.TextField(blank=True, null=True)
    created_by = models.ForeignKey(User, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    sub_dir = models.ManyToManyField('FolderName')

    def __unicode__(self):
        return self.name


class AllLog(models.Model):
    entity_name = models.CharField(max_length=50)
    baap_dir_path = models.CharField(max_length=500)
    log_type = models.CharField(max_length=20)
    action_time = models.DateTimeField(auto_now_add=True)
    baap_job = models.CharField(max_length=20, blank=True, null=True)
    action_by = models.ForeignKey(User)

    def __unicode__(self):
        return self.entity_name


class Extension(models.Model):
    extension = models.CharField(max_length=10)

    def __unicode__(self):
        return self.extension


class AllDelete(models.Model):
    entity_name = models.CharField(max_length=50)
    baap_dir_path = models.CharField("Entity_path", max_length=500)
    dir_level = models.CharField(max_length=50, blank=True, null=True)
    entity_type = models.CharField(max_length=20, default="file")
    action_time = models.DateTimeField(auto_now_add=True)
    action_by = models.ForeignKey(User)

    def directory(self):
        return self.baap_dir_path.split('media/')[1]

    def __unicode__(self):
        return self.entity_name

GroupManage.base_fields['group'] = forms.ModelChoiceField(queryset=Group.objects.all().order_by('name'))
UserManage.base_fields['user'] = forms.ModelChoiceField(queryset=User.objects.all().order_by('-username'))