import os
import shutil
from django.contrib import admin
from ocean_docs.models import (
    Job, MasterFolderType, FolderName, Extension,
    JobSubDir, AllDelete, Document, JobSubSubDir, AllLog, FolderName
)
from guardian.admin import GuardedModelAdmin, UserManage
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User, Group
from django.core.exceptions import NON_FIELD_ERRORS
from guardian.shortcuts import get_objects_for_group, get_objects_for_user 
from guardian.shortcuts import assign_perm, remove_perm
from django.contrib.auth.forms import ReadOnlyPasswordHashField
#from guardian.admin import AdminGroupObjectPermissionsForm

class ExtensionAdmin(admin.ModelAdmin):
    pass

admin.site.register(Extension, ExtensionAdmin)


class JobForm(forms.ModelForm):

    class Meta:
        exclude = ('created_by', 'baap_dir_path', )


class JobAdmin(admin.ModelAdmin):
    form = JobForm
    def save_model(self, request, obj, form, change):
        #import ipdb; ipdb.set_trace()
        obj.created_by = request.user
        obj.save()
    
admin.site.register(Job, JobAdmin)

class JobSubDirAdmin(admin.ModelAdmin):
    pass


class MasterFolderTypeForm(forms.ModelForm):

    class Meta:
        exclude = ('created_by',)

class MasterFolderTypeAdmin(admin.ModelAdmin):
    form = MasterFolderTypeForm

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

admin.site.register(MasterFolderType, MasterFolderTypeAdmin)


class FolerNameForm(forms.ModelForm):
    class Meta:
        exclude = ('created_by',)

class AdminUserManage(UserManage):
    user = forms.ModelChoiceField(queryset=User.objects.all().order_by('username'))


class FolderNameAdmin(GuardedModelAdmin):
    form = FolerNameForm
    UserManage = AdminUserManage
    list_display = ('name', 'created_by', 'created_at')
    search_fields = ('name',)
    ordering = ('-created_at',)
    date_hierarchy = 'created_at'


    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

admin.site.register(FolderName, FolderNameAdmin)


def restore_deleted(modeladmin, request, queryset):
    files = queryset.filter(entity_type='file')
    for file in files:
        Document.objects.get_or_create(
            docfile=os.path.join(file.baap_dir_path, file.entity_name),
            file_name=file.entity_name,
            dir_path=file.baap_dir_path,
            uploaded_by=request.user,
        )
        AllLog.objects.get_or_create(
            entity_name=file.entity_name,
            baap_dir_path=file.baap_dir_path.split('media')[1],
            log_type="Restored",
            action_by=request.user,
            baap_job=file.baap_dir_path.split('media')[1].split('/')[0],
        )

        file.delete()

    sub_dirs = queryset.filter(dir_level='sub_sub_job')
    for sub_dir in sub_dirs:
        JobSubSubDir.objects.get_or_create(
            name=sub_dir.entity_name,
            created_by=request.user,
            baap_dir_path=sub_dir.baap_dir_path,
        )
        full_sub_dir_path = os.path.join(
            sub_dir.baap_dir_path, sub_dir.entity_name
        )
        AllLog.objects.get_or_create(
            entity_name=sub_dir.entity_name,
            baap_dir_path=sub_dir.baap_dir_path.split('media')[1],
            log_type="Restored",
            action_by=request.user,
            baap_job=sub_dir.baap_dir_path.split('media')[1].split('/')[0],
        )
        for doc_file in os.listdir(full_sub_dir_path):
            Document.objects.get_or_create(
                docfile=os.path.join(full_sub_dir_path, doc_file),
                file_name=doc_file,
                dir_path=os.path.join(sub_dir.baap_dir_path, sub_dir.entity_name),
                uploaded_by=request.user,
            )
        sub_dir.delete()

    sub_dirs = queryset.filter(dir_level='sub_job')
    for sub_dir in sub_dirs:
        #import ipdb; ipdb.set_trace()

        sub_dir_path = os.path.join(sub_dir.baap_dir_path, sub_dir.entity_name)
        AllLog.objects.get_or_create(
            entity_name=sub_dir.entity_name,
            baap_dir_path=sub_dir.baap_dir_path.split('media')[1],
            log_type="Restored",
            action_by=request.user,
            baap_job=sub_dir.baap_dir_path.split('media')[1].split('/')[0],
        )
        job_dir = sub_dir.baap_dir_path.split('media/')[1].split('/')[0]
        job_dir = '_'.join(job_dir.split('_')[0:-1])
        sub_job = JobSubDir.objects.get_or_create(
            name=FolderName.objects.get(name=sub_dir.entity_name),
            created_by=request.user,
            baap_dir=Job.objects.get(
                name=job_dir,
            ),
            baap_dir_path=sub_dir.baap_dir_path,
        )
        for content in os.listdir(sub_dir_path):
            deep_content = os.path.join(sub_dir_path, content)
            if os.path.isdir(deep_content):
                JobSubSubDir.objects.get_or_create(
                    name=content,
                    created_by=request.user,
                    baap_dir=sub_job[0],
                    baap_dir_path=sub_dir_path,
                )
                for sub_file in os.listdir(deep_content):
                    Document.objects.get_or_create(
                        docfile=os.path.join(deep_content, sub_file),
                        file_name=sub_file,
                        dir_path=deep_content,
                        uploaded_by=request.user
                    )
            if os.path.isfile(deep_content):
                Document.objects.get_or_create(
                    docfile=deep_content,
                    file_name=content,
                    dir_path=sub_dir_path,
                    uploaded_by=request.user,
                )

        sub_dir.delete()


restore_deleted.short_description = "Restore the deleted items"

def delete_deleted(modeladmin, request, queryset):
    files = queryset.filter(entity_type='file')
    for file in files:
        file_dir = os.path.join(file.baap_dir_path, file.entity_name)
        if os.path.exists(file_dir):
            os.remove(file_dir)
        file.delete()

    sub_dirs = queryset.filter(dir_level='sub_job')
    for sub_dir in sub_dirs:
        sub_dir_path = os.path.join(sub_dir.baap_dir_path, sub_dir.entity_name)
        if os.path.exists(sub_dir_path):
            shutil.rmtree(sub_dir_path)
        sub_dir.delete()

    sub_dirs = queryset.filter(dir_level='sub_sub_job')
    for sub_dir in sub_dirs:
        sub_dir_path = os.path.join(sub_dir.baap_dir_path, sub_dir.entity_name)
        if os.path.exists(sub_dir_path):
            shutil.rmtree(sub_dir_path)
        sub_dir.delete()


delete_deleted.short_description = "Delete the deleted items"


class AllDeleteAdmin(admin.ModelAdmin):
    #exclude = ('baap_dir_path', )
    #fields = ('entity_name', 'dir_level', 'entity_type', 'action_by')
    list_display = ('entity_name', 'directory','action_by','action_time',)
    list_filter = ('entity_name','action_by','action_time',)
    search_fields = ('entity_name', 'baap_dir_path',)

    def has_add_permission(self, request):
        return False
    
    actions = [delete_deleted, restore_deleted]

admin.site.register(AllDelete, AllDeleteAdmin)


class GroupAdminForm(forms.ModelForm):
    users = forms.ModelMultipleChoiceField(
        queryset=User.objects.all(), 
        required=False,
        widget=FilteredSelectMultiple(
            verbose_name=_('Users'),
            is_stacked=False
        )
    )

    class Meta:
        model = Group
        fields = ('name', 'users', 'permissions')

    def __init__(self, *args, **kwargs):
        super(GroupAdminForm, self).__init__(*args, **kwargs)

        if self.instance and self.instance.pk:
            self.fields['users'].initial = self.instance.user_set.all()
    
    def save(self, commit=True):
        group = super(GroupAdminForm, self).save(commit=commit)
        # for no_perm_folder in FolderName.objects.all():
        #     if no_perm_folder not in self.cleaned_data['object_permissions']:
        #         remove_perm('ocean_docs.view_foldername', group, no_perm_folder)
        #         group.save()
        # for folder in self.cleaned_data['object_permissions']:
        #         assign_perm('ocean_docs.view_foldername', group, folder)
        #         group.save()
        if commit:
            group.user_set = self.cleaned_data['users']
            
        else:
            old_save_m2m = self.save_m2m
            def new_save_m2m():
                old_save_m2m()
                group.user_set = self.cleaned_data['users']
            self.save_m2m = new_save_m2m
        return group


class GroupAdmin(admin.ModelAdmin):
    form = GroupAdminForm

class UserAdminForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField(label= ("Password"),
        help_text= ("Raw passwords are not stored, so there is no way to see "
                    "this user's password, but you can change the password "
                    "using <a href=\"password/\">this form</a>."))
    object_permissions = forms.ModelMultipleChoiceField(
        required=False,
        queryset=FolderName.objects.all(), 
        widget=FilteredSelectMultiple(
            verbose_name=_('FolderName'),
            is_stacked=False
        )
    )
    class Meta:
        model = User
        fields = (
            'username', 'password', 'first_name', 'last_name', 'email', 'is_active',
            'is_staff', 'is_superuser', 'user_permissions', 'groups',
            'object_permissions', 'last_login', 'date_joined', 
        )

    def __init__(self, *args, **kwargs):
        super(UserAdminForm, self).__init__(*args, **kwargs)

        if self.instance and self.instance.pk:
            self.fields['groups'].initial = self.instance.groups.all()
            self.base_fields['object_permissions'].initial = get_objects_for_user(
                self.instance, 'ocean_docs.view_foldername')
    
    def save(self, commit=True):
        user = super(UserAdminForm, self).save(commit=commit)
        user.save()
        for no_perm_folder in FolderName.objects.all():
            if no_perm_folder not in self.cleaned_data['object_permissions']:
                remove_perm('ocean_docs.view_foldername', user, no_perm_folder)
        for folder in self.cleaned_data['object_permissions']:
                assign_perm('ocean_docs.view_foldername', user, folder)
        #add_fieldsets = ('object_permissions',)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

class UserAdmin(admin.ModelAdmin):
    change_user_form = UserAdminForm
    #readonly_fields=('password',)

#admin.site.unregister(User)
#admin.site.register(User, UserAdmin)
admin.site.unregister(Group)
admin.site.register(Group, GroupAdmin)