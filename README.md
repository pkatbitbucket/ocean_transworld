1. git clone git@bitbucket.org:pkatcoverfox/ocean_transworld.git
2. cd ocean_transworld
3. virtualenv ocean
4. source ocean/bin/activate
5. pip install -r requirements.txt
6. Need to create database if its not already there(In that machine I have created it.)
7. python manage.py migrate
8. python manage.py createsuperuser
	1. username
	2. email
	3. passward
9. python manage.py shell
	1. from django.contrib.sites.models import Site
	2. new_site = Site.objects.create(domain='127.0.0.1:8000', name='localhost')
	3. new_site.save()
	Note: If comes like already exist its good.
	Leave it already done.

	4. new_site = Site.objects.create(domain='oceantransworld.in', name='host')
	5. new_site.save()
10. python manage.py runserver
	Go to http://127.0.0.1:8000 in url it must show login page.

	You are almost done!

11. run: gunicorn -w 5 --bind 127.0.0.1:8000 ocean_transworld.wsgi:application &